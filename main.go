package main

import (
	"fmt"
	"time"

	unleash "github.com/Unleash/unleash-client-go"
)

func init() {
	unleash.Initialize(
		unleash.WithListener(&unleash.DebugListener{}),
		unleash.WithAppName("staging"),
		unleash.WithInstanceId("bp4Y_-BxFAtj4y-A2Zy8"),
		unleash.WithUrl("http://localhost:8181/api/v4/feature_flags/unleash/19"),
	)
}

func main() {
	// Client library is unstable without sleep
	time.Sleep(5 * time.Second)

	FeatureFlagName := "ci_live_trace"

	if unleash.IsEnabled(FeatureFlagName) {
		fmt.Printf("Feature flag %v is enabled\n", FeatureFlagName)
	} else {
		fmt.Printf("Feature flag %v is disabled\n", FeatureFlagName)
	}
}
